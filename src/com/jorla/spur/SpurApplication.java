package com.jorla.spur;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.jorla.spur.database.SpurDatabaseSchema;

/**
 * 
 * This initializes database helper and stores so that this can be used by other activities when ever required
 * 
 */
public abstract class SpurApplication extends Application {
	private SpurDatabaseSchema db;
	public final String APP_NAME = getAppName();
	
	public abstract String getAppName();
	public abstract String getDbName();
	public abstract int getDbVersion();
	
	@Override
	public void onCreate() {
		Log.d(APP_NAME, "APPLICATION onCreate");
		db = (SpurDatabaseSchema) OpenHelperManager.getHelper(getApplicationContext(), getDbSchemaClass());
		super.onCreate();
	}
	
	protected abstract Class<? extends SpurDatabaseSchema> getDbSchemaClass();

	@Override
	public void onTerminate() {
		Log.d(APP_NAME, "APPLICATION onTerminate");
		super.onTerminate();
	}

	public SpurDatabaseSchema getDB() {
		return this.db;
	}

	public void setDB(SpurDatabaseSchema db) {
		this.db = db;
	}
}
