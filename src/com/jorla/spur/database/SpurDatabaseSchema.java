package com.jorla.spur.database;

import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.List;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import android.content.Context;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.jorla.spur.SpurApplication;

public abstract class SpurDatabaseSchema extends OrmLiteSqliteOpenHelper {
	private static final String DEBUG_TAG = "DatabaseSchema";
	private final Context context;
	private final String dbName;
	
	public SpurDatabaseSchema(Context context) {
		super(context, 
			  ((SpurApplication) context.getApplicationContext()).getDbName(), 
			  null, 
			  ((SpurApplication) context.getApplicationContext()).getDbVersion());
		this.context = context;
		this.dbName = ((SpurApplication) context.getApplicationContext()).getDbName();
	}
	
	public abstract List<Class<? extends DataModel>> getModelClasses();
	
	@Override
	public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
		List<Class<? extends DataModel>> modelClasses = getModelClasses();
		for (Class<? extends DataModel> modelClass : modelClasses) {
			try {
				TableUtils.createTable(connectionSource, modelClass);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}


	@Override
	public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
		Log.w(DEBUG_TAG, "Upgrading database. Existing contents will be lost. ["
				+ oldVersion + "]->[" + newVersion + "]");
		List<Class<? extends DataModel>> modelClasses = getModelClasses();
		for (Class<? extends DataModel> modelClass : modelClasses) {
			try {
				TableUtils.dropTable(connectionSource, modelClass, true);
			} catch (SQLException e) {
				Log.e("DatabaseSchema drop table", e.getMessage());
			}
		}

		onCreate(db, connectionSource);
		
	}
}

