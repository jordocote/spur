package com.jorla.spur.data;

import com.google.android.gms.maps.model.Polyline;

public class SplitPolyline {
	private Polyline primaryLine = null;
	private Polyline secondaryLine = null;

	public SplitPolyline() {
		
	}
	
	public SplitPolyline(Polyline p) {
		primaryLine = p;
	}
	
	public SplitPolyline(Polyline p1, Polyline p2) {
		primaryLine = p1;
		secondaryLine = p2;
	}
	
	public Polyline getPrimaryLine() {
		return primaryLine;
	}
	
	public void setPrimaryLine(Polyline primaryLine) {
		this.primaryLine = primaryLine;
	}
	
	public Polyline getSecondaryLine() {
		return secondaryLine;
	}
	
	public void setSecondaryLine(Polyline secondaryLine) {
		this.secondaryLine = secondaryLine;
	}
	
	public void clear() {
		if (primaryLine != null) {
			primaryLine.remove();
			primaryLine = null;
		}
		if (secondaryLine != null) {
			secondaryLine.remove();
			secondaryLine = null;
		}
	}

}
