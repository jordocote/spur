package com.jorla.spur.data;

import com.google.android.gms.maps.model.LatLng;

public class LatLngTime {
	private LatLng point;
	private long timestamp;
	
	public LatLngTime(LatLng point, long timestamp) {
		this.point = point;
		this.timestamp = timestamp;
	}
	
	public LatLng getPoint() {
		return point;
	}
	public void setPoint(LatLng point) {
		this.point = point;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	
}
