package com.jorla.spur.data.path;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.PatternMatcher;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.jsambells.directions.google.DirectionsAPI;

public class PathWithLegs implements Path, Parcelable {
	private List<PathImpl> legs = new ArrayList<PathImpl>();
	protected double totalDistance = 0;
	protected boolean distancesValid = false;
	
	
    public static final Parcelable.Creator<PathWithLegs> CREATOR = new Parcelable.Creator<PathWithLegs>() {
        public PathWithLegs createFromParcel(Parcel in) {
            return new PathWithLegs(in);
        }

        public PathWithLegs[] newArray(int size) {
            return new PathWithLegs[size];
        }
    };

	
	public PathWithLegs() {
		
	}
	

	public PathWithLegs(String s) {
		distancesValid = true; // will be updated incrementally
		for (String leg : s.split("&")) {
			this.legs.add(new PathImpl(leg));
		}
	}

	public PathWithLegs(Parcel in) {
    	in.readTypedList(legs, PathImpl.CREATOR);
    	calculateDistances();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeTypedList(legs);
	}
	
	public String toString() {
		StringBuffer s = new StringBuffer();
		boolean f = false;
		for (PathImpl leg : this.legs) {
			if (f) {
				s.append('&');
			} else {
				f = true;
			}
			s.append(leg.toString());
		}
		return new String(s);
	}
	
	/*
	 * Add a leg to internal state.
	 * Don't include first point of leg if it is same as current last point.
	 * (Assume legs are all connected)
	 */
	public void addLeg(PathImpl leg) {
		PathImpl newLeg = new PathImpl(leg.getPoints());
		this.legs.add(newLeg);
		this.distancesValid = false;
	}
	
	public void addLeg(List<LatLng> points) {
		this.addLeg(new PathImpl(points));
	}
	
	/*
	 * Return the leg index for which the i'th point exists
	 */
	public int findLegIndex(int i) {
		if (i < 0) {
			throw new IndexOutOfBoundsException("findLegIndex's point parameter is less than zero");
		}
		if (i >= size()) {
			throw new IndexOutOfBoundsException("findLegIndex's point parameter is greater than length");
		}
		
		int legNumber = 0;
		int counter = 0;
		
		// iterate over legs (these have duplicate end/start points)
		for (Path leg : this.legs) {
			int legSize = leg.size();
			counter += legSize;
			if (legNumber > 0 && legSize > 0) {
				// duplicate end/start point
				counter--;
			}
			if (i < counter) {
				return legNumber;
			}
			legNumber++;
		}
		
		throw new IndexOutOfBoundsException("Somehow could not find leg index for point");
	}
	
	protected void calculateDistances() {
		this.totalDistance = 0;
		for (PathImpl leg : legs) {
			leg.calculateDistances();
			this.totalDistance += leg.getTotalDistance();
		}
		this.distancesValid = true;
	}
	
	/*
	 * Return the number of points in all the legs up to (not including) the i'th leg (0-based)
	 * Includes first point in leg (last point in previous leg)
	 */
	public int getPointsUpToLeg(int i) {
		int count = 0;
		for (int j = 0; j < i; j++) {
			PathImpl leg = legs.get(j);
			int legSize = leg.size();
			count += legSize;
			if (j > 0 && legSize > 0) {
				count--;
			}
		}
		return count;
	}
	
	public PathImpl getLastLeg() {
		return getPathLeg(this.legsCount() - 1);
	}
	
	public PathImpl getPathLeg(int i) {
		List<LatLng> legPoints = new ArrayList<LatLng>();
		legPoints.addAll(legs.get(i).getPoints());
		return new PathImpl(legPoints);
	}
	
	public List<PathImpl> getPathLegs() {
		List<PathImpl> snapshot = new ArrayList<PathImpl>();
		for (int i = 0; i < legs.size(); i++) {
			snapshot.add(getPathLeg(i));
		}
		return snapshot;
	}
	
	public void clearPathLeg(int i) {
		PathImpl pathLeg = legs.get(i);
		pathLeg.clear();
	}
	
	public void setPathLeg(int i, PathImpl pathLeg) {
		legs.set(i, pathLeg);
	}
	
	public LatLng getFirstPoint() {
		if (this.legs.size() == 0) {
			return null;
		}
		PathImpl pathLeg= this.legs.get(0);
		if (pathLeg.size() == 0) {
			return null;
		}
		return pathLeg.get(0);
	}
	
	public LatLng getLastPoint() {
		if (this.legs.size() == 0) {
			return null;
		}
		PathImpl pathLeg= this.getLastLeg();
		if (pathLeg.size() == 0) {
			return null;
		}
		return pathLeg.getLast();
	}
	
	public void clear() {
		legs.clear();
	}
	
	public void removeLeg(int i) {
		this.legs.get(i).clear();
		this.legs.remove(i);
	}
	
	public void removeLastLeg() {
		this.removeLeg(this.legs.size()-1);
	}
	
	public int legsCount() {
		return legs.size();
	}

	public double getTotalDistance() {
		double distance = 0;
		for (PathImpl pathLeg : this.legs) {
			distance += pathLeg.getTotalDistance();
		}
		return distance;
	}
	
	
	@Override
	public int describeContents() {
		return 0;
	}


	@Override
	public int size() {
		int size = 0;
		int legNum = 0;
		for (PathImpl leg : legs) {
			int legSize = leg.size();
			size += legSize;
			if (legNum > 0 && legSize > 0) {
				size--;
			}
			legNum++;
		}
		return size;
	}


	/*
	 * Return a snapshot of all the points in all the legs
	 */
	@Override
	public List<LatLng> getPoints() {
		List<LatLng> points = new ArrayList<LatLng>();
		int legNum = 0;
		for (PathImpl pathLeg : legs) {
			List<LatLng> legPoints = pathLeg.getPoints();
			if (legNum > 0) {
				// remove duplicate end/start point
				legPoints.remove(0);
			}
			points.addAll(legPoints);
			legNum++;
		}
		return points;
	}


	@Override
	public void add(LatLng point) {
		PathImpl lastLeg = this.getLastLeg();
		totalDistance -= lastLeg.getTotalDistance();
		lastLeg.add(point);
		totalDistance += lastLeg.getTotalDistance();
	}

	/*
	 * Return copy of point (so state is immutable)
	 */
	public LatLng get(int i) {
		int legIndex = findLegIndex(i);
		PathImpl leg = legs.get(legIndex);
		
		// get index of point in leg
		int pointIndex = i;
		if (legIndex > 0) {
			int pointsBeforeLeg = getPointsUpToLeg(legIndex);
			pointIndex -= pointsBeforeLeg;
			pointIndex++; // skip duplicate
		}
		
		LatLng p = leg.get(pointIndex);
		return new LatLng(p.latitude, p.longitude);
	}
	
	public LatLng getLast() {
		LatLng p = getLastLeg().getLast();
		return new LatLng(p.latitude, p.longitude);
	}

	@Override
	public double getSegmentDistance(int i) {
		int legIndex = findLegIndex(i+1);
		PathImpl leg = legs.get(legIndex);
		
		// get index of segment in leg
		int segmentIndex = i;
		// separate logic for handling offset from previous legs
		if (legIndex > 0) {
			int pointsUpToLeg = getPointsUpToLeg(legIndex);
			segmentIndex += 1 - pointsUpToLeg;
		}
		
		double dist = leg.getSegmentDistance(segmentIndex);
		return dist;
	}

	
	
}
