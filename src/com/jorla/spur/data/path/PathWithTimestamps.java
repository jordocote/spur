package com.jorla.spur.data.path;

import it.rambow.master.javautils.PolylineEncoder;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.jorla.spur.data.LatLngTime;
import com.jorla.spur.util.PathUtils;
import com.jsambells.directions.google.DirectionsAPI;

public class PathWithTimestamps extends PathImpl {
	private List<Long> timestamps = new ArrayList<Long>(); // timestamps in milliseconds
	
	
    public static final Parcelable.Creator<PathWithTimestamps> CREATOR = new Parcelable.Creator<PathWithTimestamps>() {
        public PathWithTimestamps createFromParcel(Parcel in) {
            return new PathWithTimestamps(in);
        }

        public PathWithTimestamps[] newArray(int size) {
            return new PathWithTimestamps[size];
        }
    };

		
    public PathWithTimestamps() {
    	
    }
    
    public PathWithTimestamps(String s) {
    	String[] parts = s.split("@");
    	if (parts.length != 2) {
    		Log.e("PathWithTimestamps", "Invalid serialization (splitting @): " + s);
    		return;
    	}
    	String pointsString = parts[0];
    	String timestampsString = parts[1];
    	String[] timestampParts = timestampsString.split(",");
    	    	
    	List<LatLng> points = DirectionsAPI.decodePoly(pointsString);
    	
    	if (timestampParts.length != points.size()) {
    		Log.e("PathWithTimestamps", "Invalid serialization (timestamps length != points length): " + s);
    		return;
    	}
    	
    	int i = 0;
    	for (LatLng p : points) {
    		distancesValid = true; // will be updated incrementally
    		long ts = Long.valueOf(timestampParts[i++]);
    		this.add(p, ts);
    	}
    }
        
    public PathWithTimestamps(LatLngTime... points) {
    	for (LatLngTime p : points) {
    		distancesValid = true; // will be updated incrementally
    		this.add(p);
    	}
    }
    
    public PathWithTimestamps(Parcel in) {
    	in.readTypedList(points, LatLng.CREATOR);
    	calculateDistances();  // populates segmentDistances and totalDistance
    }
    
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeTypedList(points);
		
		Long[] timeArray = timestamps.toArray(null);
		long[] primitiveTimeArray = ArrayUtils.toPrimitive(timeArray);
		dest.writeLongArray(primitiveTimeArray);
	}
	
	public String toString() {
		StringBuilder out = new StringBuilder();
		
		// add latlng points
		out.append(PolylineEncoder.createEncodings(this.points, 1));
		
		// separator
		out.append("@");
		
		// add timestamps
		int firstPartSize = out.length();
		for (long t : timestamps) {
			if (out.length() > firstPartSize) {
				out.append(",");
			}
			out.append(t);
		}

		return out.toString();
	}
	

	public void add(LatLng point) {
		throw new IllegalArgumentException("PathWithTimestamp::add() requires a motherfucking timestamp.");
	}
	
	/*
	 * Add a new point at the end. Incrementally updates distances.
	 */
	public void add(LatLng point, long timestamp) {
		super.add(point);
		timestamps.add(timestamp);
	}
	
	public void add(LatLngTime point) {
		add(point.getPoint(), point.getTimestamp());
	}
	
	public LatLngTime getLatLngTime(int i) {
		return new LatLngTime(points.get(i), timestamps.get(i));
	}
	
	public Iterator<LatLngTime> iterator() {
		return new Iterator<LatLngTime>() {
			private int i = 0;

			@Override
			public boolean hasNext() {
				return i < size() - 1;
			}

			@Override
			public LatLngTime next() {
				return getLatLngTime(i);
			}

			@Override
			public void remove() {
				// nah
			}
		};
	}

	public void setPoint(int i, LatLng point, long timestamp) {
		super.setPoint(i, point);
		timestamps.set(i, timestamp);
	}
	
	@Override
	public void remove(int i) {
		super.remove(i);
		timestamps.remove(i);
	}
	
}
