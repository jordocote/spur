package com.jorla.spur.data.path;

import com.google.android.gms.maps.model.Marker;
import com.jorla.spur.data.SplitPolyline;

public class PathGraphic {
	private SplitPolyline splitPolyline;
	private Marker endMarker;
	
	public PathGraphic() {
		splitPolyline = new SplitPolyline();
	}
	
	public SplitPolyline getSplitPolyline() {
		return splitPolyline;
	}
	public void setSplitPolyline(SplitPolyline splitPolyline) {
		this.splitPolyline = splitPolyline;
	}
	public Marker getEndMarker() {
		return endMarker;
	}
	public void setEndMarker(Marker endMarker) {
		this.endMarker = endMarker;
	}
	
	
	public void clear() {
		if (this.splitPolyline != null) {
			this.splitPolyline.clear();
		}
		if (this.endMarker != null) {
			this.endMarker.remove();
			this.endMarker = null;
		}
	}
	
}
