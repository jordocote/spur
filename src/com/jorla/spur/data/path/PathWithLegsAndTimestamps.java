package com.jorla.spur.data.path;

import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;

public class PathWithLegsAndTimestamps extends PathWithLegs {
	private List<PathWithTimestamps> legsWithTime;
	
	public PathWithLegsAndTimestamps() {
		this.legsWithTime = new ArrayList<PathWithTimestamps>();
	}

	public PathWithLegsAndTimestamps(String s) {
		this();
		distancesValid = true; // will be updated incrementally
		for (String leg : s.split("&")) {
			//addLegInternal(new PathWithTimestamps(leg));
		}
	}

	public PathWithLegsAndTimestamps(Parcel in) {
		this();
    	in.readTypedList(this.legsWithTime, PathWithTimestamps.CREATOR);
    	calculateDistances();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeTypedList(this.legsWithTime);
	}
	
	public String toString() {
		StringBuffer s = new StringBuffer();
		boolean f = false;
		for (PathWithTimestamps leg : this.legsWithTime) {
			if (f) {
				s.append('&');
			} else {
				f = true;
			}
			s.append(leg.toString());
		}
		return new String(s);
	}
	
//??
	protected List<? extends PathImpl> getLegs() {
		return this.legsWithTime;
	}

	//implement rest of incompatible functions that use this.legs
	
	// create abstract PathWithLegs that has shared functions 
	// ie, those that just need a generic path and dont refer to pathimpl specifically
}
