package com.jorla.spur.data.path;
import it.rambow.master.javautils.PolylineEncoder;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.jorla.spur.util.PathUtils;
import com.jsambells.directions.google.DirectionsAPI;

public class PathImpl implements Path, Parcelable {
	protected List<LatLng> points = new ArrayList<LatLng>();
	protected List<Double> segmentDistances = new ArrayList<Double>();
	protected double totalDistance = 0;
	protected boolean distancesValid = false;

    public static final Parcelable.Creator<PathImpl> CREATOR = new Parcelable.Creator<PathImpl>() {
        public PathImpl createFromParcel(Parcel in) {
            return new PathImpl(in);
        }

        public PathImpl[] newArray(int size) {
            return new PathImpl[size];
        }
    };

		
    public PathImpl() {
    	
    }
    
    public PathImpl(String s) {
    	this(DirectionsAPI.decodePoly(s));
    }
    
    public PathImpl(List<LatLng> points) {
    	this.points = points;
    	this.calculateDistances();
    }
    
    public PathImpl(LatLng... points) {
    	distancesValid = true; // will be updated incrementally
    	for (LatLng p : points) {
    		this.add(p);
    	}
    }
    
    public PathImpl(Parcel in) {
    	in.readTypedList(points, LatLng.CREATOR);
    	calculateDistances();  // populates segmentDistances and totalDistance
    }
    
	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeTypedList(points);
	}
	
	public String toString() {
		return PolylineEncoder.createEncodings(this.points, 1);
	}
	
	
	public double getTotalDistance(){
		if (!this.distancesValid) {
			this.calculateDistances();
		}
		return totalDistance;
	}
	
	protected void calculateDistances() {
		if (this.distancesValid) {
			return;
		}
		totalDistance = 0;
		segmentDistances.clear();
		LatLng iterPoint;
		LatLng lastIterPoint;
		if (size() > 1){
			Iterator<LatLng> iterPnt = this.points.iterator();
			lastIterPoint = iterPnt.next();
			while (iterPnt.hasNext()){
				iterPoint = iterPnt.next();
				double dist = PathUtils.calculateDistance(lastIterPoint, iterPoint);
				segmentDistances.add(dist);
				totalDistance += dist;
				lastIterPoint = iterPoint;
			}
		}
		distancesValid = true;
	}
	
	public int size() {
		return points.size();
	}

	/*
	 * Return snapshot of current points
	 */
	public  List<LatLng> getPoints() {
		List<LatLng> snapshot = new ArrayList<LatLng>(points);
		return snapshot;
	}

	/*
	 * Modify existing point. Invalidates distances.
	 */
	public void setPoint(int i, LatLng point) {
		this.points.set(i, point);
		this.distancesValid = false;
	}
	
	/*
	 * Replace all points with a new set. Invalidates distances.
	 */
	public void setPoints(List<LatLng> mPoints) {
		this.points = mPoints;
		this.distancesValid = false;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	/*
	 * Add a new point at the end. Incrementally updates distances.
	 */
	public void add(LatLng point) {
		points.add(point);
		LatLng prior = get(size()-1);
		double dist = PathUtils.calculateDistance(prior, point);
		segmentDistances.add(dist);
		totalDistance += dist;
	}
	
	/*
	 * Return copy of point (so state is immutable)
	 */
	public LatLng get(int i) {
		LatLng p = points.get(i);
		return new LatLng(p.latitude, p.longitude);
	}
	
	public LatLng getLast() {
		LatLng p = points.get(points.size() - 1);
		return new LatLng(p.latitude, p.longitude);
	}
	
	public double getSegmentDistance(int i) {
		if (!this.distancesValid) {
			this.calculateDistances();
		}
		double dist = this.segmentDistances.get(i);
		return dist;
	}

	public void remove(int i) {
		this.points.remove(i);
		this.distancesValid = false;
	}
	
	
	public void clear() {
		this.points.clear();
		this.segmentDistances.clear();
		this.totalDistance = 0;
		this.distancesValid = false;
	}
}
