package com.jorla.spur.data.path;

import java.util.Iterator;
import java.util.List;

import com.google.android.gms.maps.model.LatLng;
import com.jorla.spur.util.PathUtils;

public interface Path {
	/*
	 * Constructor from serialized version (suggested)
	 */
	// public Path(String serialization);
	
	/*
	 * Return serialization of path
	 */
	public String toString();
	
	/*
	 * Return the total distance of the path. Calculate distance if needed
	 */
	public double getTotalDistance();
			
	/*
	 * Number of points in the path
	 */
	public int size();

	/*
	 * Get a List of all the points
	 */
	public  List<LatLng>  getPoints();
	
	/*
	 * Add a new point at the end. Incrementally updates distances.
	 */
	public void add(LatLng point);
	
	/*
	 * Get a point
	 */
	public LatLng get(int i);
	
	/*
	 * Get the last point
	 */
	public LatLng getLast();
	
	/*
	 * Get the distance between points i and (i+1)
	 */
	public double getSegmentDistance(int i);
	
	public void clear();
	
}
