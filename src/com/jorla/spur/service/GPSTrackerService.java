package com.jorla.spur.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

public class GPSTrackerService extends Service{
    public static final String INTENT_LOCATION_UPDATE = "com.jorla.spur.LOCATION_UPDATE";
    
	// Listens for location updates then broadcasts via "LOCATION_UPDATE" intent action
	private Context context;
	private boolean isStarted;
	GPSTrackerTask gpsTrackerTask = new GPSTrackerTask();

	public GPSTrackerService(Context context){
		this.context = context;
	}

	// Getters and Setters
	public boolean isStarted() {
		return isStarted;
	}
	public void setStarted(boolean isStarted) {
		this.isStarted = isStarted;
	}

	/**
	 * Class for clients to access.  Because we know this service always
	 * runs in the same process as its clients, we don't need to deal with
	 * IPC.
	 */
	public class GPSTrackerServiceBinder extends Binder {
		GPSTrackerService getService() {
			return GPSTrackerService.this;
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreate() {
		Log.i("GPSTrackerService", "in onCreate now...");
		isStarted = false;
	}


	@Override
	public void onStart(Intent intent, int startId) {
		Log.i("GPSTrackerService", "in onStart");
		if (startService()){
			Log.i("GPSTrackerService", "Listening for location ...");
			setStarted(true);
		}else{
			setStarted(false);
		}
	}

	public boolean startService() {
		try {
			gpsTrackerTask.execute();
			return true;
		} catch (Exception error) {
			return false;
		}
	}

	@Override
	public boolean stopService(Intent intent) {
		try {
			gpsTrackerTask.cancel(true);
			gpsTrackerTask.cancelTracking();
			return true;
		} catch (Exception error) {
			return false;
		}
	}
	
	@Override
	public void onDestroy(){
		Log.i("GPSTrackerService", "in onDestroy");
		Intent intent = new Intent("com.jorla.spur.LOCATION_UPDATE");
		stopService(intent);
		gpsTrackerTask.cancelTracking();
		setStarted(false);
	}


	private class GPSTrackerTask extends AsyncTask<Void, Void, Void> { 
		private boolean gpsProvider;
		private boolean networkProvider;
		private Location locationVal;
		private LocationManager locationManager;
		private LocationListener locationListener = new LocationListener(){  // Setup locationListener 
			@Override
			public void onLocationChanged(Location location) {
				handleLocationUpdate(location);
			}
			@Override
			public void onProviderDisabled(String provider) {
			}
			@Override
			public void onProviderEnabled(String provider) {
			}
			@Override
			public void onStatusChanged(String provider, int status,
					Bundle extras) {
			}
		};

		/** Custom methods */
		public void cancelTracking(){ //Called from activity by stopService(intent) --(which calls in service)--> onDestroy() --(which calls in asynctask)--> cancelTracking()
			locationManager.removeUpdates(locationListener);
		}

		@Override 
		protected Void doInBackground(Void... params) { 
			while(!isCancelled() /* TODO: or if finishline endpoint is reached*/){ 
				/* GPSTracking Thread will run until you tell it to stop by 
				 * changing tracking to 0 by calling method cancelVal();
				 * Will also remove locationListeners from locationManager*/
				//Log.i("GPSTracker","asyncTask still running...");
				try {
					// keep the thread alive
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return null; 

		}

		private void handleLocationUpdate(Location location){ // Called by locationListener override.
			locationVal = location;
			publishProgress();
		}

		@Override
		protected void onCancelled(){
			super.onCancelled();
			this.cancelTracking();
			stopSelf();
		}

		@Override
		protected void onPostExecute(Void result) { 
			//Performed after execution, stopSelf() kills the thread
			stopSelf();
		}

		@Override
		protected void onPreExecute(){ 
			// Performed prior to execution, setup location manager
			locationManager = (LocationManager)  context.getSystemService(Context.LOCATION_SERVICE);
			networkProvider=true;
			if(gpsProvider==true){
				locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
			}
			if(networkProvider==true){
				locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
			}
		}

		@Override
		protected void onProgressUpdate(Void... v){ //called when publishProgress() is invoked within asynctask
			//On main ui thread, perform desired updates, potentially broadcast the service use notificationmanager
			/** NEED TO BROADCAST INTENT VIA sendBroadCast(intent); */
			Intent intent = new Intent("com.jorla.spur.LOCATION_UPDATE");
			//Put extras here if desired
			intent.putExtra("ACCURACY", locationVal.getAccuracy()); // float double double long int
			intent.putExtra("LATITUDE", locationVal.getLatitude());
			intent.putExtra("LONGITUDE", locationVal.getLongitude());
			intent.putExtra("TIMESTAMP", locationVal.getTime());
			intent.putExtra("ALTITUDE",locationVal.getAltitude());
			Log.i("GPSTracker","onProgressUpdate found location:" + "("
					+ locationVal.getLatitude() + "," 
					+ locationVal.getLongitude() + "), time:" + locationVal.getTime());
			context.sendBroadcast(intent); //broadcasting update. use GPSLocationReceiver to receive LOCATION_UPDATE
		}
	}
}