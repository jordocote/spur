package com.jorla.spur.util;

import it.rambow.master.javautils.PolylineEncoder;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.ImageView;

import com.google.android.gms.maps.model.LatLng;

public class RouteSnapshot {
    Bitmap bitmap;
    private Handler handlerthumb;
    private ImageView thumb;
	
	public void createSnapshot(Context context, int width, int height, List<LatLng> points, Handler handler) {
		this.handlerthumb = handler;
		ConnectivityManager connMgr = (ConnectivityManager) 
            context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            new DownloadWebpageText().execute(
            		getMapUrl(width, height, points)
            	);
        } else {
            Log.i("n","No network connection available.");
        }
	}
	

	public static String getMapUrl(int width, int height, List<LatLng> path) {
		final String pipe = "%7C";
		
		// max url size is 2048. allow ~1000 for the path. (each point is about 10 char)
		int step = 1;
		if (path.size() > 100) {
			step = path.size() / 100;
		}
		String encodedPath = PolylineEncoder.createEncodings(path, step);
		
	    String url =  "http://maps.googleapis.com/maps/api/staticmap?"
	            + "&size=" + width + "x" + height
	            + "&maptype=roadmap&sensor=true";
	    if (path.size() > 1) {
	    	String weight = String.valueOf((int)(Math.floor(0.075 * Math.sqrt(width*height))));
	    	url += "&path=color:0xff0000ff"+pipe+"weight:" + weight;
//		    for (LatLng point : path) {
//		    	url += pipe + point.latitude + "," + point.longitude;
//		    }
	    	url += pipe + "enc:" + Uri.encode(encodedPath);
		    
	    	LatLng start = path.get(0);
	    	LatLng end = path.get(path.size()-1);
	    	String startPoint = start.latitude + "," + start.longitude;
	    	String startPoint2 = start.latitude + "," + (start.longitude+0.000001);
	    	url += "&path=color:0x00ff00ff"+pipe+"weight:50"+pipe+startPoint+pipe+startPoint2;
	    	String endPoint = end.latitude + "," + end.longitude;
	    	String endPoint2 = end.latitude + "," + (end.longitude+0.000001);
	    	url += "&path=color:0x0000ffff"+pipe+"weight:50"+pipe+endPoint+pipe+endPoint2;
	    	
	    }
	    
//    	Log.i("url",url);
//		Log.i("path size", String.valueOf(path.size()));
//		Log.i("step", String.valueOf(step));
//		Log.i("encoded size", String.valueOf(encodedPath.length()));
//		Log.i("url size", String.valueOf(url.length()));
	    return url;
	}
	

    // Uses AsyncTask to create a task away from the main UI thread. This task takes a 
    // URL string and uses it to create an HttpUrlConnection. Once the connection
    // has been established, the AsyncTask downloads the contents of the webpage as
    // an InputStream. Finally, the InputStream is converted into a string, which is
    // displayed in the UI by the AsyncTask's onPostExecute method.
    private class DownloadWebpageText extends AsyncTask<String, Integer, Bitmap>  {
       @Override
       protected Bitmap doInBackground(String... urls) {
             
           // params comes from the execute() call: params[0] is the url.
           
                return downloadBitmap(urls[0]);
           
       }
       // onPostExecute displays the results of the AsyncTask.
       @Override
       protected void onPostExecute(Bitmap result) {
           Bundle b = new Bundle();
           b.putParcelable("bitmap", result);
           Message m = handlerthumb.obtainMessage();
           m.setData(b);
           m.sendToTarget();
      }
   }
    

    Bitmap downloadBitmap(String url) {
        final int IO_BUFFER_SIZE = 4 * 1024;

        // AndroidHttpClient is not allowed to be used from the main thread
        boolean no_async_task = false;
        final HttpClient client = (no_async_task) ? new DefaultHttpClient() :
            AndroidHttpClient.newInstance("Android");
        final HttpGet getRequest = new HttpGet(url);

        try {
            HttpResponse response = client.execute(getRequest);
            final int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK) {
                Log.w("ImageDownloader", "Error " + statusCode +
                        " while retrieving bitmap from " + url);
                return null;
            }

            final HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream inputStream = null;
                try {
                    inputStream = entity.getContent();
                    // return BitmapFactory.decodeStream(inputStream);
                    // Bug on slow connections, fixed in future release.
                    return BitmapFactory.decodeStream(new FlushedInputStream(inputStream));
                } finally {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                    entity.consumeContent();
                }
            }
        } catch (IOException e) {
            getRequest.abort();
            Log.w("a", "I/O error while retrieving bitmap from " + url, e);
        } catch (IllegalStateException e) {
            getRequest.abort();
            Log.w("a", "Incorrect URL: " + url);
        } catch (Exception e) {
            getRequest.abort();
            Log.w("a", "Error while retrieving bitmap from " + url, e);
        } finally {
            if ((client instanceof AndroidHttpClient)) {
                ((AndroidHttpClient) client).close();
            }
        }
        return null;
    }

    /*
     * An InputStream that skips the exact number of bytes provided, unless it reaches EOF.
     */
    static class FlushedInputStream extends FilterInputStream {
        public FlushedInputStream(InputStream inputStream) {
            super(inputStream);
        }

        @Override
        public long skip(long n) throws IOException {
            long totalBytesSkipped = 0L;
            while (totalBytesSkipped < n) {
                long bytesSkipped = in.skip(n - totalBytesSkipped);
                if (bytesSkipped == 0L) {
                    int b = read();
                    if (b < 0) {
                        break;  // we reached EOF
                    } else {
                        bytesSkipped = 1; // we read one byte
                    }
                }
                totalBytesSkipped += bytesSkipped;
            }
            return totalBytesSkipped;
        }
    }
}
