package com.jorla.spur.util;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.mutable.MutableInt;

import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.jorla.spur.data.path.Path;
import com.jorla.spur.data.path.PathImpl;

public class PathUtils {
	private static final int EarthRadius = 6371; //km


	/*
	 * Compute distance between two points
	 */
	public static double calculateDistance(LatLng pointA, LatLng pointB){
		/*
		 * using Haversine formula to calculate distance 'as the crow flies' 
		between two points across spherical globe known as earth
		 *
		 */
		double dLat = (pointB.latitude - pointA.latitude) * Math.PI/180;
		double dLon = (pointB.longitude-pointA.longitude) * Math.PI/180;
		double lat1 = pointA.latitude * Math.PI/180;
		double lat2 = pointB.latitude * Math.PI/180;

		double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
		        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		double distance = EarthRadius * c;
		return distance * 0.621371;
	}
	
	public static double roundTwoDecimals(double d) {
	    DecimalFormat twoDForm = new DecimalFormat("#.##");
	    return Double.valueOf(twoDForm.format(d));
	}
	
	

	public static LatLng interpolatePoint(LatLng begin, LatLng end, double ratio) {
		// find middle latlng point
		double midLat = begin.latitude + (end.latitude - begin.latitude) * ratio;
		double midLong = begin.longitude + (end.longitude - begin.longitude) * ratio;
		return new LatLng(midLat, midLong);
	}
	
	public static LatLng getPointAtDistance(Path path, double distance) {
		return getPointAtDistance(path, distance, new MutableInt());
	}
	
	/*
	 * Returns the point on the path which is specified distance from start
	 * @param containingSegment Will be set to the index of the segment that the point is found at. Sets -1 if point is out of bounds.
	 */
	public static LatLng getPointAtDistance(Path path, double distance, MutableInt containingSegment){
		List<LatLng> points = path.getPoints();
		double totalDistance = path.getTotalDistance();
		int size = path.size();
		
		double trailDistance = 0.0;
		double lastSegmentDistance = 0.0;
		double ratioInSegment = 0;
		
		if (size == 0) {
				throw new IndexOutOfBoundsException("Path is empty");
		} else if (size == 1) {
			// if the trail is only one point, return that point
			containingSegment.setValue(-1);
			return points.get(0);
		} else if (distance == 0) {
			// if distance is 0 return start point
			containingSegment.setValue(0);
			return points.get(0);
		} else if (distance >= totalDistance) {
			// if distance passed in is greater than total distance of the trail
			// then get the end point on the trail
			containingSegment.setValue(-1);
			return points.get(size-1);
		} else {
			for (int i=0;i<size;i++){
				lastSegmentDistance = trailDistance;
				double segmentDistance = path.getSegmentDistance(i);
				trailDistance += segmentDistance;
				if (trailDistance >= distance){
					// the point is in segment i
					containingSegment.setValue(i);
					ratioInSegment = (distance - lastSegmentDistance) / segmentDistance;
					return interpolatePoint(points.get(i), points.get(i+1), ratioInSegment);
				}
			}
			// reached the end, return last point (should be unreachable code)
			containingSegment.setValue(-1);
			return points.get(size-1);
		}
	}

	public static List<LatLng> getPointRange(Path path, int start, int end) {
		List<LatLng> range = path.getPoints().subList(start, end + 1);		
		return range;
	}
}
