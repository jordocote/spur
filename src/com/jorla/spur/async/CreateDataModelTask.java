package com.jorla.spur.async;

import java.sql.SQLException;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.jorla.spur.SpurApplication;
import com.jorla.spur.database.DataModel;
import com.jorla.spur.database.SpurDatabaseSchema;



/*
 * 
 * inserts model to database. done on a worker thread other than ui thread
 * 
 */
	public class CreateDataModelTask extends AsyncTask<DataModel, Integer, DataModel> {
		private OnTaskCompleted listener;
		
		SpurApplication app;
		public CreateDataModelTask(Context context,OnTaskCompleted listener){
			app = (SpurApplication) context.getApplicationContext(); 
		}
		
		//private final ProgressDialog dialog = new ProgressDialog(this);
		// can use UI thread here
		protected void onPreExecute() {
			Log.i("CreateRaceTask","Inserting race data...");
		}

		protected DataModel doInBackground(DataModel... datamodels) {

			DataModel datamodel = null;
    		int count = datamodels.length;
			for (int i = 0; i < count; i++) {
				try{
    				publishProgress((int) ((i / (float) count) * 100));
    				// Escape early if cancel() is called
    				if (isCancelled()) break;
	            	SpurDatabaseSchema db = app.getDB();
	            	Dao<DataModel, String> dao = DaoManager.createDao(db.getConnectionSource(), datamodels[i].getClass());
					dao.createOrUpdate(datamodels[i]);	            
	        	}catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					OpenHelperManager.releaseHelper();
				}
				datamodel = datamodels[i];
				
			}
			return datamodel;
		}
		
		protected void onProgressUpdate(Integer... progress) {
			int count = progress.length;
			for (int i = 0; i < count; i++) {
				setProgress(progress[i]);
			}
		}

		private void setProgress(Integer integer) {
			// TODO Auto-generated method stub
			
		}

		protected void onPostExecute(DataModel result) {
			//listener.onTaskCompleted(result);
		}

		public interface OnTaskCompleted{
		    void onTaskCompleted(DataModel datamodel);
		}


	}
    