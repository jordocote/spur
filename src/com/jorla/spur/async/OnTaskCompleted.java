package com.jorla.spur.async;

import com.jorla.spur.database.DataModel;

public interface OnTaskCompleted{
    void onTaskCompleted(DataModel datamodel);
}