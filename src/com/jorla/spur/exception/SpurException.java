package com.jorla.spur.exception;

public class SpurException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7247875648663447908L;

	public enum ErrorCode {
		UNKNOWN,
		HASH_ISSUE,
		AUTH_PROTO_VERSION_NOT_SUPPORTED,
		API_ACCOUNT_REQUIRED,
		ACCOUNT_AUTH_ISSUE,
		ACCOUNT_NOT_FOUND,
	};
	
	private ErrorCode errorCode;

	public SpurException() {
		super();
		this.errorCode = ErrorCode.UNKNOWN;
	}
	
	public SpurException(ErrorCode errorCode, String message, Throwable cause) {
		super(message, cause);
		this.setErrorCode(errorCode);
	}

	public SpurException(ErrorCode errorCode, String message) {
		super(message);
		this.setErrorCode(errorCode);
	}

	public ErrorCode getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(ErrorCode errorCode) {
		this.errorCode = errorCode;
	}
}
