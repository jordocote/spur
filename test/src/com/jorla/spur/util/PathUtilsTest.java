package com.jorla.spur.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.mutable.MutableInt;

import junit.framework.TestCase;

import com.google.android.gms.maps.model.LatLng;
import com.jorla.spur.data.path.Path;
import com.jorla.spur.data.path.PathImpl;
import com.jorla.spur.data.path.PathWithLegs;



public class PathUtilsTest extends TestCase {
	private static final List<LatLng> points = new ArrayList<LatLng>();;
	private PathWithLegs pathWithLegs;
	private PathImpl pathImpl;
	
	@Override
    protected void setUp() throws Exception {
    	points.clear();
    	points.add(new LatLng(40.7225,-73.9501));
    	points.add(new LatLng(41.8254,-72.6324));
    	points.add(new LatLng(41.5235,-72.2664));
    	points.add(new LatLng(41.0235,-71.2664));
    	
    	pathImpl = new PathImpl(points);
    	
    	pathWithLegs = new PathWithLegs();
    	pathWithLegs.addLeg(points.subList(0, 2));
    	pathWithLegs.addLeg(points.subList(1, 4));
    }

	@Override
    protected void tearDown() throws Exception {
        
    }

    public void testGetPointRange() {
    	runGetPointRange(pathImpl);
    	runGetPointRange(pathWithLegs);
    }
    
    protected void runGetPointRange(Path path) {
    	List<LatLng> pointRange = PathUtils.getPointRange(path, 0, 3);
    	assertEquals(points.subList(0, 4), pointRange);
    	
    	pointRange = PathUtils.getPointRange(path, 0, 2);
    	assertEquals(points.subList(0, 3), pointRange);
    	
    	pointRange = PathUtils.getPointRange(path, 2, 3);
    	assertEquals(points.subList(2, 4), pointRange);
    	
    	try {
    		pointRange = PathUtils.getPointRange(path, 0, 10);
    		fail("Method accepted out of bounds end point");
    	} catch (Exception e) {
    		assertTrue(true);
    	}
    	
    	try {
    		pointRange = PathUtils.getPointRange(path, -1, 2);
    		fail("Method accepted out of bounds start point");
    	} catch (Exception e) {
    		assertTrue(true);
    	}
    }
    
    public void testGetPointAtDistance() {
    	runGetPointAtDistance(pathImpl);
    	runGetPointAtDistance(pathWithLegs);
    }
    
	protected void runGetPointAtDistance(Path path) {	
    	// test in 1st segment
    	MutableInt segmentReturned = new MutableInt();
    	LatLng begin = path.get(0);
    	LatLng end = path.get(1);
    	double segmentDist = PathUtils.calculateDistance(begin, end);
    	double dist = 50.0;
    	double ratio = dist / segmentDist;
    	LatLng point = PathUtils.getPointAtDistance(path, dist, segmentReturned);
    	assertEquals(PathUtils.interpolatePoint(begin, end, ratio), point);
    	assertEquals(0, segmentReturned.intValue());
    	
    	// test in 2nd segment
    	begin = path.get(1);
    	end = path.get(2);
    	double segmentOneDist = segmentDist;
    	segmentDist = PathUtils.calculateDistance(begin, end);
    	dist = 14.0;
    	ratio = dist / segmentDist;
    	point = PathUtils.getPointAtDistance(path, segmentOneDist + dist, segmentReturned);
    	assertEquals(PathUtils.interpolatePoint(begin, end, ratio), point);
    	assertEquals(1, segmentReturned.intValue());

    	// test distance too long
    	point = PathUtils.getPointAtDistance(path, 9999.9, segmentReturned);
    	assertEquals(path.getLast(), point);
    	assertEquals(-1, segmentReturned.intValue());
    	
    	// test distance zero
    	point = PathUtils.getPointAtDistance(path, 0, segmentReturned);
    	assertEquals(path.get(0), point);
    	assertEquals(0, segmentReturned.intValue());
    }
}
