package com.jorla.spur.data.path;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import com.google.android.gms.maps.model.LatLng;



public class PathWithLegsTest extends TestCase {
	private static final List<LatLng> points = new ArrayList<LatLng>();;
	private PathWithLegs pathWithLegs;
	
	@Override
    protected void setUp() throws Exception {
    	points.clear();
    	points.add(new LatLng(40.7225,-73.9501));
    	points.add(new LatLng(41.8254,-72.6324));
    	points.add(new LatLng(41.5235,-72.2664));
    	points.add(new LatLng(41.0235,-71.2664));
    	
    	pathWithLegs = new PathWithLegs();
    	pathWithLegs.addLeg(points.subList(0, 2));
    	pathWithLegs.addLeg(points.subList(1, 4));
    }

	@Override
    protected void tearDown() throws Exception {
        
    }
	
	public void testGetPoint() {
		assertEquals(new LatLng(40.7225,-73.9501), pathWithLegs.get(0));
		assertEquals(new LatLng(41.8254,-72.6324), pathWithLegs.get(1));
		assertEquals(new LatLng(41.5235,-72.2664), pathWithLegs.get(2));
		assertEquals(new LatLng(41.0235,-71.2664), pathWithLegs.get(3));
	}

	public void testFindLegIndex() {
		assertEquals(0, pathWithLegs.findLegIndex(0));
		assertEquals(0, pathWithLegs.findLegIndex(1));
		assertEquals(1, pathWithLegs.findLegIndex(2));
		assertEquals(1, pathWithLegs.findLegIndex(3));
	}
}
