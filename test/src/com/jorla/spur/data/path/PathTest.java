package com.jorla.spur.data.path;


import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import com.jorla.spur.data.path.Path;
import com.jorla.spur.data.path.PathImpl;
import com.jorla.spur.data.path.PathWithLegs;
import com.jorla.spur.util.PathUtils;

import com.google.android.gms.maps.model.LatLng;



public class PathTest extends TestCase {
	private static final List<LatLng> points = new ArrayList<LatLng>();;
	private PathWithLegs pathWithLegs;
	private PathImpl pathImpl;
	
	@Override
    protected void setUp() throws Exception {
    	points.clear();
    	points.add(new LatLng(40.7225,-73.9501));
    	points.add(new LatLng(41.8254,-72.6324));
    	points.add(new LatLng(41.5235,-72.2664));
    	points.add(new LatLng(41.0235,-71.2664));
    	
    	pathImpl = new PathImpl(points);
    	
    	pathWithLegs = new PathWithLegs();
    	pathWithLegs.addLeg(points.subList(0, 2));
    	pathWithLegs.addLeg(points.subList(1, 4));
    }

	@Override
    protected void tearDown() throws Exception {
        
    }

    public void testGetSegmentDistance() {
    	runGetSegmentDistance(pathImpl);
    	runGetSegmentDistance(pathWithLegs);
    }
    
	protected void runGetSegmentDistance(Path path) {
    	assertEquals(PathUtils.calculateDistance(path.get(0), path.get(1)), path.getSegmentDistance(0));
    	assertEquals(PathUtils.calculateDistance(path.get(1), path.get(2)), path.getSegmentDistance(1));
    	assertEquals(PathUtils.calculateDistance(path.get(2), path.get(3)), path.getSegmentDistance(2));
    }
    
    public void testGetTotalDistance() {
    	runGetTotalDistance(pathImpl);
    	runGetTotalDistance(pathWithLegs);
    }
    	
    protected void runGetTotalDistance(Path path) {
    	double totalDist = 0.0;
    	totalDist += PathUtils.calculateDistance(path.get(0), path.get(1));
    	totalDist += PathUtils.calculateDistance(path.get(1), path.get(2));
    	totalDist += PathUtils.calculateDistance(path.get(2), path.get(3));
    	assertEquals(totalDist, path.getTotalDistance());
    }
    
    
}
